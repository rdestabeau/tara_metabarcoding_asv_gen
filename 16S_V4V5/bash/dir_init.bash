#!/bin/bash

mkdir -p \
	16S_V4V5 \
	16S_V4V5/outputs/reads/fwd/dada_filtered \
	16S_V4V5/outputs/reads/rev/dada_filtered \
	16S_V4V5/outputs/taxo_assignment \
	16S_V4V5/outputs/asv_table \
	16S_V4V5/outputs/quality_plot/dada \
	16S_V4V5/outputs/quality_plot/fastqc \
	16S_V4V5/outputs/quality_plot/fastqc/fwd \
	16S_V4V5/outputs/quality_plot/fastqc/rev \
	16S_V4V5/outputs/error_plot \
	16S_V4V5/outputs/demultiplex \
	16S_V4V5/log \
	16S_V4V5/tmp \
	16S_V4V5/manifests \
	16S_V4V5/data \
	16S_V4V5/awk \
	16S_V4V5/outputs/refdb

