#!/bin/bash
#
#SBATCH --partition long
#SBATCH --cpus-per-task 1
#SBATCH --mem 5GB

source ${CONFIG}

cat ${CONFIG} > tmp/test.txt

# One job per fastq file
sbatch --wait --array 1-$(cat manifests/primer_trimming.txt | wc -l)%30 \
	--export=CONFIG=${CONFIG} \
	analyses/01_primer_trimming_b.bash

