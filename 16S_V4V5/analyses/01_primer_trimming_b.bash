#!/bin/bash
#
#SBATCH --partition fast
#SBATCH --cpus-per-task 1
#SBATCH --mem 50GB

bash bash/primer_trimming.bash $SLURM_ARRAY_TASK_ID $CONFIG

