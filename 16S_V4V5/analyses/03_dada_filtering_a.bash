#!/bin/bash

source $CONFIG

# One job per fastq file
sbatch --array 1-$(cat ${MANIFEST} | wc -l)%30 \
	--export=CONFIG=${CONFIG} \
	analyses/03_dada_filtering_b.bash
