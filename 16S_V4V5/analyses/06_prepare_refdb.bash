#!/bin/bash

########################################################
# Download the references db
########################################################

# SILVA
wget -N http://www2.decipher.codes/Classification/TrainingSets/SILVA_SSU_r138_2019.RData -P data/refdb/


########################################################
# Reference database formating
########################################################

# vsearch
bash bash/format_for_vsearch.bash data/refdb/SILVA_138.1_SSURef_NR99_tax_silva.fasta.gz ' ' ';'

# Formating classification training set for R use
module load r/4.1.1
export TMPDIR=tmp/
Rscript R/rda_to_rds.R data/refdb/SILVA_SSU_r138_2019.RData


# idtaxa
ln data/refdb/SILVA_SSU_r138_2019.rds outputs/refdb/

########################################################
# Reference database trimming
########################################################

bash/ref_db_trimm.bash config/ref_db_trimm_silva_16sv4v5.cfg
