#!/bin/bash
#
#SBATCH --partition fast
#SBATCH --mem 20GB

module load r/4.1.1

TMPINPUT=$(awk "NR==$SLURM_ARRAY_TASK_ID" tmp/chunk_files.txt)

source ${CONFIG}

OUTPUT=$(echo $TMPINPUT | \
        awk -v outputdir=$OUTPUTDIR -F"/" '{sub(/.fas(ta)*(\.gz)*/,".idtaxa.out.gz",$NF)
                        print outputdir"/"$NF}')

Rscript R/idtaxa_assign.R $TMPINPUT $THREADS $REFDBIDTAXA $OUTPUT

