#Generate a summary of fastqc quality contol in a .txt file

library("fastqcr")

# For R1
list.files("outputs/quality_plot/fastqc/fwd/")
qc_agr <- qc_aggregate("outputs/quality_plot/fastqc/fwd", progressbar = T)
e <- summary(qc_agr)
d <- qc_stats(qc_agr)

wt =write.table(d,"tmp/results_fastqc_fwd.txt", sep="\t", row.names = T)

# For R2
list.files("outputs/quality_plot/fastqc/rev/")
qc_agr <- qc_aggregate("outputs/quality_plot/fastqc/rev", progressbar = T)
e <- summary(qc_agr)
d <- qc_stats(qc_agr)

wt =write.table(d,"tmp/results_fastqc_rev.txt", sep="\t", row.names = T)