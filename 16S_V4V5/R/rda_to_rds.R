
args <- commandArgs(TRUE)
rdata_path <- args[1]
output <- sub("\\.RData$",".rds",rdata_path)

load(rdata_path, ex <- new.env())
tmp <- get(names(ex)[1],envir=ex)
saveRDS(tmp,file=output)