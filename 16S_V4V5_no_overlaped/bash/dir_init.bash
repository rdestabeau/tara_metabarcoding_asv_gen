#!/bin/bash

mkdir -p \
	outputs/reads/fwd/dada_filtered \
	outputs/reads/rev/dada_filtered \
	outputs/taxo_assignment \
	outputs/asv_table \
	outputs/quality_plot/dada \
	outputs/quality_plot/fastqc \
	outputs/quality_plot/fastqc/fwd \
	outputs/quality_plot/fastqc/rev \
	outputs/error_plot \
	outputs/demultiplex \
	log \
	tmp \
	manifests \
	data \
	awk \
	outputs/refdb

