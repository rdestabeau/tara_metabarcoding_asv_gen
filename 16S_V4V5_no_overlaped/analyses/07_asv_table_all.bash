#!/bin/bash

export TMPDIR=tmp/
module load r/4.1.1
Rscript R/asv_table_all.R


wait 

# Copying asv table from 16s_v4v5 and 18s_v9 in the same repository: 16s_v4v5_no_overlaped/outputs/asv_table/
cp /shared/projects/tara_single_cell_metab/16S_V4V5/outputs/asv_table/asv_table_16sv4v5_idtaxa.tsv \
    /shared/projects/tara_single_cell_metab/16S_V4V5_no_overlaped/outputs/asv_table/

cp /shared/projects/tara_single_cell_metab/18S_V9/outputs/asv_table/asv_table_18sv9_idtaxa.tsv \
    /shared/projects/tara_single_cell_metab/16S_V4V5_no_overlaped/outputs/asv_table/

wait 

# R script to concatenate all necessary information for ecological interpretation 
# The output is a .tsv file proposing up to five couple association procaryota/eukaryota 
# eukaryote/eukaryote for each isolat number.
# Also the last column 'expected' refers to what has been isolated.

export TMPDIR=tmp/
module load r/4.1.1
Rscript R/further_analysis.R