#!/bin/bash

source ${CONFIG}

Rscript R/dada_table.R $PROJECT_NAME $VERSION


zcat outputs/asv_table/16s_v4v5_non_overlaped_dada2_v1.0.filtered.table.tsv.gz | \
    awk 'NR>1 {print $1, $2}' | \
        sort |\
        uniq | \
        awk '{print ">"$1"\n"$2}' > manifests/asv_seq.fasta
