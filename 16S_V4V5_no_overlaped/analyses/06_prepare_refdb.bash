#!/bin/bash

########################################################
# Download the references db
########################################################

# pr2
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU_UTAX.fasta.gz -P data/refdb/
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU.decipher.trained.rds -P data/refdb/

########################################################
# Reference database formating
########################################################

# vsearch
bash bash/format_for_vsearch.bash data/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz ';tax=' ','


# idtaxa
ln data/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds outputs/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds
ln data/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz outputs/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz

########################################################
# Reference database trimming
########################################################

bash/ref_db_trimm.bash config/ref_db_trimm_pr2_16sv4v5_non_overlaped.cfg
