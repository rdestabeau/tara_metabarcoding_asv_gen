#!/bin/bash

source ${CONFIG}

NRUNS=$(cat ${MANIFEST} | wc -l)

if [ "$NRUNS" -gt 1 ]
then
	sbatch --array 1-$(cat ${MANIFEST} | wc -l)%30 \
		--cpus-per-task ${THREADS} \
		--export=CONFIG=${CONFIG} \
		analyses/04_dada_denoising_b.bash
else
	sbatch --array 1 \
		--cpus-per-task ${THREADS} \
		--export=CONFIG=${CONFIG} \
		analyses/04_dada_denoising_b.bash
fi
