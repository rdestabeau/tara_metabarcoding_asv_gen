#!/bin/bash

###################################################################
# script name   : make.bash
# description   : This script produces an ASV table using dada2
# usage         : sbatch make.sh but better to run the lines one
#                 by one and check manualy the outputs
# author	: Nicolas Henry
# contact	: nicolas.henry@cnrs.fr
###################################################################

# Folders for the outputs, log files and
# temporary files are created

# bash bash/dir_init.bash


# ############################################
# # Dada2 denoising
# ############################################

#Create a manifest listing the runs to be analyzed

# echo -e 'L001_Cut1' > manifests/dada_denoising.txt

# run denoising
# sbatch --export=CONFIG="config/dada_denoising.cfg" \
# 	analyses/04_dada_denoising_a.bash

# echo "\
# sample denoisedF.read denoisedR.read merged.read \
# denoisedF.seq denoisedR.seq merged.seq nochim.read nochim.seq\
# " | tr ' ' '\t' > log/dada_denoising.log

# wait 

# cat log/*_dada_denoising.log | sort -k1 >> log/dada_denoising.log
# rm -f log/*_dada_denoising.log

# ############################################
# # ASV table assembling
# ############################################

#assemble an ASV table

# sbatch --export=CONFIG="config/dada_table.cfg" \
# 	analyses/05_dada_table.bash



# ############################################
# # Get database, formating and trimming
# ############################################

# sbatch analyses/06_prepare_refdb.bash

# ###########################################
# #Taxonomic assignment
# ###########################################

# sbatch -W --export=CONFIG="config/taxo_assign_pr2.cfg" \
# 	analyses/06_taxo_assign_a.bash

# # ## concatenate outcome in one file

# sbatch analyses/07_asv_table_all.bash
