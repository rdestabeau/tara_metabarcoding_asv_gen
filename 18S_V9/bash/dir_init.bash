#!/bin/bash

mkdir -p \
	18S_V9 \
	18S_V9/outputs/reads/fwd/dada_filtered \
	18S_V9/outputs/reads/rev/dada_filtered \
	18S_V9/outputs/taxo_assignment \
	18S_V9/outputs/asv_table \
	18S_V9/outputs/quality_plot/dada \
	18S_V9/outputs/quality_plot/fastqc \
	18S_V9/outputs/error_plot \
	18S_V9/outputs/demultiplex \
	18S_V9/log \
	18S_V9/tmp \
	18S_V9/manifests \
	18S_V9/data \
	18S_V9/awk \
	18S_V9/outputs/refdb
