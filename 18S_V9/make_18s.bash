#!/bin/bash

# Folders for the outputs, log files and
# temporary files are created

#bash bash/dir_init.bash


############################################
# Sample demplutiplex
############################################

# Run the first command to demultiplex data from archive/fastq to outputs/demultiplex 
# and also rename the files
#sbatch analyses/00_demultiplex.bash


############################################
# Primer trimming
############################################

# Create a manifest file listing
# the files (fastqs) to work with

# ls outputs/demultiplex/ | \
#         awk -v path=outputs/demultiplex \
#                 'BEGIN{FS="_"}
#                 /fastq\.gz$/{
#                 sample = $1"_"$2
#                 fwd = path"/"$1"_"$2"_R2.fastq.gz"
#                 rev = path"/"$1"_"$2"_R1.fastq.gz"
#                 print sample,fwd,rev
#                 }' | \
#         sort | \
#         uniq > "manifests/primer_trimming.txt"

# # Remove the primers

# sbatch --export=CONFIG="config/primer_trimming.cfg" \
#         analyses/01_primer_trimming_a.bash


# #Gather logs

# echo "\
# sample status in_reads in_bp too_short too_long \
# too_many_n out_reads w/adapters qualtrim_bp \
# out_bp w/adapters2 qualtrim2_bp out2_bp\
# " | tr ' ' '\t' > log/primer_trimming.log


# wait

# cat log/*_primer_trimming.log >> log/primer_trimming.log

# rm -f log/*_primer_trimming.log

# ############################################
# # Quality assesment
# ############################################

#  #Generate quality plots
#  sbatch --export=CONFIG="config/quality_plot.cfg" \
#  	analyses/02_quality_plot.bash

# ############################################
# # Dada2 filtering
# ############################################

# #manifest for filtering, keep only files with more than 0 reads
# awk '$8>0 && NR>1 {print $1}' log/primer_trimming.log \
# 	> manifests/dada_filtering.txt

# run filtering
# sbatch --export=CONFIG="config/dada_filtering.cfg" \
# 	analyses/03_dada_filtering_a.bash

# # Gather logs
# echo "sample in_reads out_reads" | tr ' ' '\t' > log/dada_filtering.log


# wait

# cat log/*_dada_filtering.log | sort -k2nr >> log/dada_filtering.log
# rm -f log/*_dada_filtering.log

# ############################################
# # Dada2 denoising
# ############################################

# #Create a manifest listing the runs to be analyzed

# echo -e 'Cut1' > manifests/dada_denoising.txt

# run denoising
# sbatch --export=CONFIG="config/dada_denoising.cfg" \
# 	analyses/04_dada_denoising_a.bash

# echo "\
# sample denoisedF.read denoisedR.read merged.read \
# denoisedF.seq denoisedR.seq merged.seq nochim.read nochim.seq\
# " | tr ' ' '\t' > log/dada_denoising.log

# wait

# cat log/*_dada_denoising.log | sort -k1 >> log/dada_denoising.log
# rm -f log/*_dada_denoising.log

# # ############################################
# # # ASV table assembling
# # ############################################

 #assemble an ASV table
# sbatch --export=CONFIG="config/dada_table.cfg" \
# 	analyses/05_dada_table.bash


# ############################################
# # Get database, formating and trimming
# ############################################

# sbatch analyses/06_prepare_refdb.bash

# ############################################
# # Taxonomic assignment
# ############################################

# sbatch --export=CONFIG="config/taxo_assign.cfg" \
# 	analyses/06_taxo_assign_vsearch_a.bash

# sbatch analyses/07_asv_table_all.bash
