TI_546	GAACCAGTACTCCCT-AGACTTCTCAGGTTG
TI_547	GAACCAGTACTCCCT-GGATGTCTTCGCTTG
TI_550	GAACCAGTACTCCCT-TCCTGAACACAGTTG
TI_551	AACTCGCGCTACCCT-CTTTGCACTTTGTTG
TI_552	TGATAATGCACGCCT-TACTTGCCACGGTTG
TI_553	GAACCAGTACTCCCT-AAGCCTCTACGATTG
TI_554	AACTCGCGCTACCCT-TGACACGACATCTTG
TI_561	TACCAGGATTGCCCT-AGACTTCTCAGGTTG
TI_563	TACCAGGATTGCCCT-GGATGTCTTCGCTTG
TI_567	TACCAGGATTGCCCT-TCCTGAACACAGTTG
TI_570	TACCAGGATTGCCCT-AAGCCTCTACGATTG
TI_576	TACCAGGATTGCCCT-TACTTGCCACGGTTG
TI_579	TGATAATGCACGCCT-AGACTTCTCAGGTTG
TI_580	TACCAGGATTGCCCT-GCATAAACGACTTTG
TI_582	TGATAATGCACGCCT-GCATAAACGACTTTG
TI_584	TGATAATGCACGCCT-GGATGTCTTCGCTTG
TI_585	TACCAGGATTGCCCT-CTTTGCACTTTGTTG
TI_586	TACCAGGATTGCCCT-TGACACGACATCTTG
TI_587	GGTTGTAAGTGTCCT-AGACTTCTCAGGTTG
TI_588	GGTTGTAAGTGTCCT-GGATGTCTTCGCTTG
TI_591	GGTTGTAAGTGTCCT-TCCTGAACACAGTTG
TI_593	GGTTGTAAGTGTCCT-AAGCCTCTACGATTG
TI_594	GGTTGTAAGTGTCCT-TACTTGCCACGGTTG
TI_595	GGTTGTAAGTGTCCT-GCATAAACGACTTTG
TI_596	GGTTGTAAGTGTCCT-CTTTGCACTTTGTTG
TI_597	GGTTGTAAGTGTCCT-TGACACGACATCTTG
TI_611	CAACGAACCATCCCT-AGACTTCTCAGGTTG
TI_613	TGATAATGCACGCCT-CTTTGCACTTTGTTG
TI_614	TGATAATGCACGCCT-TGACACGACATCTTG
TI_615	TAGTGATGACCACCT-AGACTTCTCAGGTTG
TI_616	CAACGAACCATCCCT-GGATGTCTTCGCTTG
TI_617	CAACGAACCATCCCT-TCCTGAACACAGTTG
TI_619	CAACGAACCATCCCT-AAGCCTCTACGATTG
TI_620	TAGTGATGACCACCT-GGATGTCTTCGCTTG
TI_623	CAACGAACCATCCCT-TACTTGCCACGGTTG
TI_624	TAGTGATGACCACCT-TCCTGAACACAGTTG
TI_625	CAACGAACCATCCCT-GCATAAACGACTTTG
TI_626	TAGTGATGACCACCT-AAGCCTCTACGATTG
TI_627	CAACGAACCATCCCT-CTTTGCACTTTGTTG
TI_629	CAACGAACCATCCCT-TGACACGACATCTTG
TI_640	TGATAATGCACGCCT-TCCTGAACACAGTTG
TI_643	TAGTGATGACCACCT-TACTTGCCACGGTTG
TI_646	TAGTGATGACCACCT-GCATAAACGACTTTG
TI_656	GAACCAGTACTCCCT-TACTTGCCACGGTTG
TI_657	GAACCAGTACTCCCT-GCATAAACGACTTTG
TI_659	TAGTGATGACCACCT-CTTTGCACTTTGTTG
TI_683	TAGTGATGACCACCT-TGACACGACATCTTG
TI_684	ACAGCCACCCATCCT-AGACTTCTCAGGTTG
TI_685	ACAGCCACCCATCCT-GGATGTCTTCGCTTG
TI_695	ACAGCCACCCATCCT-TCCTGAACACAGTTG
TI_699	ACAGCCACCCATCCT-AAGCCTCTACGATTG
TI_701	ACAGCCACCCATCCT-TACTTGCCACGGTTG
TI_705	ACAGCCACCCATCCT-GCATAAACGACTTTG
TI_716	GAACCAGTACTCCCT-CTTTGCACTTTGTTG
TI_725	TGATAATGCACGCCT-AAGCCTCTACGATTG
TI_735	GAACCAGTACTCCCT-TGACACGACATCTTG
TI_801	ACAGCCACCCATCCT-CTTTGCACTTTGTTG
TI_804	ACAGCCACCCATCCT-TGACACGACATCTTG
TI_806	TATGTTGACGGCCCT-AGACTTCTCAGGTTG
TI_810	TATGTTGACGGCCCT-GGATGTCTTCGCTTG
TI_813	TATGTTGACGGCCCT-TCCTGAACACAGTTG
TI_815	TATGTTGACGGCCCT-AAGCCTCTACGATTG
TI_818	TATGTTGACGGCCCT-TACTTGCCACGGTTG
TI_819	TATGTTGACGGCCCT-GCATAAACGACTTTG
TI_824	TATGTTGACGGCCCT-CTTTGCACTTTGTTG
TI_826	TATGTTGACGGCCCT-TGACACGACATCTTG
TI_827	CGAGTATACAACCCT-AGACTTCTCAGGTTG
TI_828	CGAGTATACAACCCT-GGATGTCTTCGCTTG
TI_833	CGAGTATACAACCCT-TCCTGAACACAGTTG
TI_838	CGAGTATACAACCCT-AAGCCTCTACGATTG
TI_841	CGAGTATACAACCCT-TACTTGCCACGGTTG
TI_842	CGAGTATACAACCCT-GCATAAACGACTTTG
TI_843	CGAGTATACAACCCT-CTTTGCACTTTGTTG
TI_844	CGAGTATACAACCCT-TGACACGACATCTTG
TI_845	TACACCTTACCTCCT-AGACTTCTCAGGTTG
TI_846	TACACCTTACCTCCT-GGATGTCTTCGCTTG
TI_849	TACACCTTACCTCCT-TCCTGAACACAGTTG
TI_850	TACACCTTACCTCCT-AAGCCTCTACGATTG
TI_856	TACACCTTACCTCCT-TACTTGCCACGGTTG
TI_859	TACACCTTACCTCCT-GCATAAACGACTTTG
TI_867	TACACCTTACCTCCT-CTTTGCACTTTGTTG
TI_869	TACACCTTACCTCCT-TGACACGACATCTTG
TI_870	CGTTCAAGCTAGCCT-AGACTTCTCAGGTTG
TI_871	CGTTCAAGCTAGCCT-GGATGTCTTCGCTTG
TI_872	CGTTCAAGCTAGCCT-TCCTGAACACAGTTG
TI_873	CGTTCAAGCTAGCCT-AAGCCTCTACGATTG
TI_874	CGTTCAAGCTAGCCT-TACTTGCCACGGTTG
TI_876	CGTTCAAGCTAGCCT-GCATAAACGACTTTG
TI_877	CGTTCAAGCTAGCCT-CTTTGCACTTTGTTG
TI_878	CGTTCAAGCTAGCCT-TGACACGACATCTTG
TI_882	AACTCGCGCTACCCT-AGACTTCTCAGGTTG
TI_883	AACTCGCGCTACCCT-GGATGTCTTCGCTTG
TI_884	AACTCGCGCTACCCT-TCCTGAACACAGTTG
TI_885	AACTCGCGCTACCCT-AAGCCTCTACGATTG
TI_886	AACTCGCGCTACCCT-TACTTGCCACGGTTG
TI_887	AACTCGCGCTACCCT-GCATAAACGACTTTG
