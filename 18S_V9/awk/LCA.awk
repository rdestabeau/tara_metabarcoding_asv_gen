BEGIN{FS="\t"; OFS="\t"}

NR==FNR {taxodict[$1]=$2;next}

$1!=amplicon{
	if (FNR > 1){
		output=sep=""
		for (i=1;i<=n;i++) {
			output=output sep taxo[i]
			sep=";"
		}
		print amplicon,maxscore,minscore,refs,output
	}
	amplicon=$1;maxscore=$2;minscore=$2*threshold/100;refs=$3
	n=split(taxodict[$3],taxo,";")
	next
}

$2 >= minscore {
	refs= refs","$3
	split(taxodict[$3],tmp,";")
	for (i=1;i<=n;i++) {
		if (taxo[i]!=tmp[i]){
			taxo[i]="*"
		}
	}
}

END{
	output=sep=""
	for (i=1;i<=n;i++) {
		output=output sep taxo[i]
		sep=";"
	}
	print amplicon,maxscore,minscore,refs,output
}
