#!/bin/bash

source $CONFIG

SPLE=$(awk "NR==$SLURM_ARRAY_TASK_ID" $MANIFEST)

Rscript R/dada_filtering.R $TRUNC_F $TRUNC_R $MAX_EE $SPLE
