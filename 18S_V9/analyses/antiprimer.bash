#!/bin/bash

function revcomp(){
    [[ -z "${1}" ]] && { echo "error: empty string" ; exit 1 ;}
    local nucleotides="acgturykmbdhvswACGTURYKMBDHVSW"
    local complements="tgcaayrmkvhdbswTGCAAYRMKVHDBSW"
    tr "${nucleotides}" "${complements}" <<< "${1}" | rev
}

#TACACACCGCCC
#       donne complement:
#ATGTGTGGCGGG
#       donne reverse:
#GGGCGGTGTGTA