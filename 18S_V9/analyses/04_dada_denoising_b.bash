#!/bin/bash
#
#SBATCH --partition fast
#SBATCH --mem 150GB

source $CONFIG

RUN=$(awk "NR==$SLURM_ARRAY_TASK_ID" $MANIFEST)

Rscript R/dada_denoising.R $RUN $THREADS
