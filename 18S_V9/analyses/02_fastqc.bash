#!/bin/bash

module load fastqc/0.11.9

####
# Contrôle qualité des raws reads avec fastqc
####
dirproj1="outputs/reads/fwd"
dirproj2="outputs/reads/rev"
dirout1="outputs/quality_plot/fastqc/fwd"
dirout2="outputs/quality_plot/fastqc/rev"

#QC Read 1 de chaque échantillon
fastqc $dirproj1/*.gz -o $dirout1

#QC Read 2 de chaque échantillon
fastqc $dirproj2/*.gz -o $dirout2

# Generate fastqc summary file:
module load r/4.1.1
export TMPDIR=tmp/
Rscript R/results_fastqc.R

rm -f $dirout1/*.zip
rm -f $dirout2/*.zip

rm -f $dirout1/*.html
rm -f $dirout2/*.html
