#!/bin/bash


########################################################
# Download the references db
########################################################

# PR2
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU_UTAX.fasta.gz -P data/refdb/
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU.decipher.trained.rds -P data/refdb/


# ########################################################
# # Reference database formating
# ########################################################

# vsearch
bash/format_for_vsearch.bash data/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz ';tax=' ','

# Formating classification training set for R use
module load r/4.1.1
export TMPDIR=tmp/
Rscript R/rda_to_rds.R data/refdb/SILVA_SSU_r138_2019.RData



# # idtaxa
ln data/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds outputs/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds
ln data/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz outputs/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz


########################################################
# Reference database trimming
########################################################

bash/ref_db_trimm.bash config/ref_db_trimm_pr2_18sv9.cfg
