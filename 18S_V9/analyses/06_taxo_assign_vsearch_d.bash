#!/bin/bash

source ${CONFIG}

# concatenate the results into one file
find ${OUTPUTDIR} \
        -name "*.vsearch.out.lca.gz" \
        -type f \
        -exec cat {} + > "outputs/taxo_assignment/${PROJALIAS}.${REFDBALIAS}.vsearch.out.lca.${THRESHOLD}.gz"

# concatenate the results into one file
find ${OUTPUTDIR} \
        -name "*.vsearch.out.besthit.gz" \
        -type f \
        -exec cat {} + > "outputs/taxo_assignment/${PROJALIAS}.${REFDBALIAS}.vsearch.out.besthit.gz"

# concatenate the results into one file
find ${OUTPUTDIR} \
        -name "*.idtaxa.out.gz" \
        -type f \
        -exec cat {} + > "outputs/taxo_assignment/${PROJALIAS}.${REFDBALIAS}.idtaxa.out.lca.gz"

rm -f tmp/chunks/*
rm -f tmp/chunk_files.txt
