#!/bin/bash
#
#SBATCH --partition fast
#SBATCH --mem 20GB

TMPINPUT=$(awk "NR==$SLURM_ARRAY_TASK_ID" tmp/chunk_files.txt)

echo ${TMPINPUT}
echo ${CONFIG}

bash/vsearch_LCA.bash ${CONFIG} ${TMPINPUT}
