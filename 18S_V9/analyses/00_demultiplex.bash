#!/bin/bash

module load cutadapt/3.1

R1="archive/fastq/17Di-R05-pur_S1_L001_R1_001.fastq.gz"
R2="archive/fastq/17Di-R05-pur_S1_L001_R2_001.fastq.gz"

awk 'NR>1 {print $2}' archive/V9di1701map.txt| \
    sort | \
    uniq | \
    awk '{print ">"$1"\n^"$1}' > tmp/barcodesfwd.fasta

awk 'NR>1 {print $3}' archive/V9di1701map.txt| \
    sort | \
    uniq | \
    awk '{print ">"$1"\n^"$1}' > tmp/barcodesrev.fasta

cutadapt -e 0.15 --no-indels \
    --discard-untrimmed \
    -g file:tmp/barcodesfwd.fasta \
    -G file:tmp/barcodesrev.fasta \
    -o outputs/demultiplex/{name1}-{name2}_R1.fastq.gz \
    -p outputs/demultiplex/{name1}-{name2}_R2.fastq.gz \
   ${R1} ${R2}

# R script to get the isolat number for each sample
module load r/4.1.1
Rscript R/get_correspondance.R


# Rename after demultiplex
awk 'NR>1 {
    isolat = $2
    fwd = $3
    rev = $4
    print isolat"\t"fwd"-"rev}' tmp/sample_table.txt | \
    sort | \
    uniq > "tmp/rename.txt"

path="outputs/demultiplex"


while read isolat sequence; do
var=$(ls $path | \
    grep "${sequence}" | grep "_R1")
var2="$path/$var"
mv $var2 ${var2/$sequence/$isolat}

var=$(ls $path | \
    grep "${sequence}" | grep "_R2")
var2="$path/$var"
mv $var2 ${var2/$sequence/$isolat}

done < tmp/rename.txt


# Run the second command to rename correctly your files: 
    # If you still want reads where no barcodes were found, feel free to remove this option:
        # "--discard-untrimmed"
    # Else, you can remove thoses reads after demultiplexing with cutadapt with this commande:
        # srun find outputs/demultiplex/ -type f -name "*unknown*" -exec rm -f {}

# s'assurer qu'on ait bien que des "TI-" dans le noms des fichiers:
# on fera ça au moment du trimming ls fastq/ | grep "TI-"
