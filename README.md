 **Table Of Contents**

[[_TOC_]]




# Project description : Highlighting plankton symbiosis associations from Tara Ocean expedition

This project leans on previous work from Nicolas Henry, research engineer at Station Biologique de Roscoff and Tara data bio-analyst.


You'll discover several script adapted to sequenced rDNA single cell metabarcoding mostly symbiosis between cyanobacteria and eukaryotes, isolated from the scientific expedition Tara Ocean samples (2009-2013).

## Link to metabarcoding pipeline by Nicolas Henry & dada2 pipeline tutorial (V1.16)

https://gitlab.sb-roscoff.fr/nhenry/abims-metabarcoding-pipeline-dada2

https://benjjneb.github.io/dada2/tutorial.html

As mentionned in [benjjneb](https://benjjneb.github.io/dada2/tutorial.html) dada2 tutorial, before starting pipeline the data needs to meet certain criteria:

* *Samples have been demultiplexed, i.e. split into individual per-sample fastq files.*
* *Non-biological nucleotides have been removed, e.g. primers, adapters, linkers, etc.*
* *If paired-end sequencing data, the forward and reverse fastq files contain reads in matched order.*


## Input data info
Three repository: **16S_V4V5** , **18S_V9** and **16S_V4V5_no_overlaped** are at the root of the project.
In the three of them you will find a bash script named `make_16s.bash`, `make_18s.bash` and `make_16s_no_overlaped.bash`. They will
guide you through the pipeline. 

It is recommended to:
* create repository for your own data in **archive/fastq/** and adapt path if necessary;
* run the `sbatch` command line one after the other so you are sure you're not missing anything and avoid conflicts.

# Bio analysis

## Primer Trimming

### 16S V4V5
Reads have already been demultiplex. The `make_16s.bash` starts with removing the primers:

* Forward: ^GTGYCAGCMGCCGCGGTAA
* Reverse: ^CCGYCAATTYMTTTRAGTTT
* Reference: https://doi.org/10.1111/1462-2920.13023

### 18S V9
Reads have not been demultiplex yet. The first command line `00_demultiplex.bash` will
demultiplex files R1, R2 and will rename the fastq files with its respective isolat number.

Then, as the script continues, the primers will be removed.
* Forward: ^TACACACCGCCC
* Reverse: ^TCYGCAGGTTCACCTAC
* Reference: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0006372#s2

*`^` specifies to cutadapt the sequence primers is meant to only be at the beginning of t   he read.*


## Quality control

Once the samples have been demultiplexed, the primers and barcodes sequences have been removed, we can proceed to the quality control.
The script   `02_quality_plot.bash` calls a script in **R/** named `quality_plots.R`. The output is a quality plot for the forward and the reverse reads present in **outputs/quality_plot/dada/**.

## Filtering and trimming

After inspecting the reads quality profiles you can determine a position to truncate the forward and reverse reads. Remember, you have 
already modified the size of reads by removing primers and you want to retrieve the full fragment size of the sequence of interest.

Naturally, forward and reverse reads must still overlap after truncation ( at least 20 common nucleotides). In our case, fragment size are 130b for 18S V9 and 380b for 16S V4V5; we will truncate at position :

* **16S V4V5** :
    * Forward: 230
    * Reverse: 200
* **18S V9** :
    * Forward: 200
    * Reverse: 150

## Denoising
During omic data acquisition, there is an integration of a few biases mostly during PCR and sequencing present in reads. The script named
`dada_denoising.R` located in directory **R/** is using a function called *learnErrors*. It will use an amount of reads as training set to train the model. The model will be applied to all the data set so the only variation we find later on, is biological by nature. This step is essential when we want to conceive exact sequence variants.

### Option
`trimOverhang=TRUE`: [Optional](https://rdrr.io/bioc/dada2/man/mergePairs.html). *"Default FALSE. If TRUE, "overhangs" in the alignment between the forwards and reverse read are trimmed off. "Overhangs" are when the reverse read extends past the start of the forward read, and vice-versa, as can happen when reads are longer than the amplicon and read into the other-direction primer region."*


### Plot LearnError Rates:
[learnErrors:](http://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf). *"Error rates are learned by alternating between sample inference and error rate estimation until convergence. Sample inferences is performed by the dada function. Error rate estimation is performed by errorEstimationFunction. The output of this function serves as input to the dada function call as the err parameter."*

At the end of this script we have two files (forward, reverse) in **outputs/error_plot/**. It corresponds to the error rate for each possible transition (A -> C, A -> G etc...).

Each points is an observed error rate for each consensus quality score.
The black line shows the estimated error rate after convergence of the machine learning algorithm.
The red line shows the expected error rate according to the nominal definition of the Q-score. 


[Q-score](https://github.com/benjjneb/dada2/issues/310): *"reflects the probability that a base is misread : p_err= 1/3"*
    Q = -10log_10 (p_err)
Ideally, the estimated error rates (black line) should match the observed rates (points). Quality must increase as the error rate decreases. 


[Argument nominalQ](http://www.bioconductor.org/packages/devel/bioc/manuals/dada2/man/dada2.pdf): *"(Optional). Default FALSE. If TRUE, plot the expected error rates (red line) if quality scores exactly matched their nominal definition: Q = -10 log10(p_err)."*


## ASV table
The script `R/dada_table.R` allows the 16S V4V5 and the 18S V9 to construct the amplicon sequence variant table in repository **outputs/asv_table**.
This outcome is only adapted to specific files names:
* 16S : TI-XXX_LXXX_CutX
* 18S : TI_XXX_CutX

## Taxonomic assignment

### Database preparation
Both following database will be downloaded with script `06_prepare_refdb.bash` and slightly adapted for our use.

* 16s : Version [138.1 ](https://www.arb-silva.de/no_cache/download/archive/release_138.1/Exports/) of SILVA, reference database in rRNA.
* 18s : Version [v4.14.0](https://github.com/pr2database/pr2database/releases) of PR2 Database. Also, a .rds file is downloaded, it is the training set for the IDTAXA method. 


### Algorithms
Three approaches are used for the taxonomic assignment :

* [IDTAXA](https://microbiomejournal.biomedcentral.com/articles/10.1186/s40168-018-0521-5) : *"a novel approach to taxonomic classification that employs principles from machine learning to reduce over classification errors".*

Two other methods are based on [vsearch](https://manpages.org/vsearch) package. *"Vsearch implements an extremely fast implementation of the Needleman-Wunsch algorithm"*. It aligns each query sequences to the adapted database for 16s or 18s. With a scoring system using match, mismatch and gap and many more options, the alogrithm will assign the exact sequence variant to a taxonomic identification as a result and with a certain confidence. 

* Best hit : simply using vsearch and looking for best hit comparisons.
* lca : 


### Output
The output of the script `07_asv_table_all.bash` and the all pipeline is two .txt files saved in **outputs/asv_table/** named (for 16s samples):

* `asv_table_16s_v4v5_idtaxa.tsv` :  
    * The first column refers to the `amplicon` identification;
    * The second one refers to the `taxonomy` using IDTAXA; 
    * `score_idtaxa` reveals the score associated, the confidence related to the output of the IDTAXA algorithm;
    * Column named `clade` is added to simplify reading about taxonomic results. It is also important to mentionned: for 16S_V4V5, not only prokaryots have been sequenced. In fact there are mitochondria, chloroplasts, archaea, eukaryotes as well, from multiple potential sources (free-floating dna, contaminant for example). Script in `R/asv_table.R` looks for specific taxonomy associated with Bacteria in ID TAXA nomenclature such as mitochondria and chloroplast;
    * You can get exact variant sequence in column `sequence`;
    * `total` is the sum of all amplicon sequence variant in each isolat;
    * `spread`
    * The rest of the asv table is the abundance of every single sequence variant in each isolat.

* `asv_table_16s_v4v5_all.tsv` : the new columns corresponds to the taxonomy using best hit and lca 99 methods. The corresponding `score`  is also mentionned. It might be used when not being sure about IDTAXA output or simply be more confidence.




## 16S_V4V5_non_overlaped
The goal here is to fetch with the V4V5 data set eukaryote individuals:
To do so we modify the `R/dada_denoising.R `script. (we add option [justConcatenate=TRUE](https://rdrr.io/bioc/dada2/man/mergePairs.html) to function `mergePairs`) .

We also modify the script:` bash/vsearch_LCA.bash` by adding the option [--iddef 0](https://manpages.org/vsearch) to avoid counting in the score that gaps will introduce (insertion between the two ends of the reads).

We will only assign taxonomy with pr2 database because we want to focus on finding eukaryotes and will merge this new table with the one previously generated (`outputs/asv_table/16sv4v5_idtaxa.tsv` and `outputs/asv_table/asv_table_18sv9_idtaxa.tsv`).


### Filtering asv table
For the 16s, we have negative controls. They correspond to empty well in the sequencing step (in which there is no biological material, only reactive and primers). 

Nevertheless, reads for these controls have been sequenced. They correspond to species identified by IDTAXA as being for most bacteria (Gammaproteobacteria, Alphaproteobacteria for example.):
* It is necessary to remove from this table the amplicons for which there are reads in controls. They correspond to contaminants if:
    * They are bacteria related to the human epidermis;
    * Marine/aquatic habitat is not known for this bacteria;
    * Unclassified.


* Contamination can come from 3 potentials sources: 
    * A fragment of DNA generated by an old PCR; 
    * A false positive: may appear during a "sample-to-sample" contamination (DNA is quite volatile); 
    * Contamination from the consumables used.


* Then we must remove everything that we do not want to see appear and keep what we are looking for  (column named `rank_before_filtering` is used to classify amplicon before removing anything. That way we have information about how an amplicon is abundant before any filtering).
    * 16s, we will only be interested in the bacteria; 
    * 18s, we will be interested only in eukaryotes; 
    * 16s without overlap, we will be interested only in eukaryotes as well.


### Output

The output is a .tsv file, saved in `finalresult/document/`. Ordered by isolat number, it is proposing up to five couple association procaryota/eukaryota.
This table is necessary information for ecological interpretation.
Also the last column `expected` refers to what has been isolated. 


Adding 16s_non_overlaped method in this asv table has two major interests:
* A confirmation of the presence of the euk identified by IDTAXA with the V9 dataset;
* Get the eukaryote identification for isolat number that sequenced plate that did not work;
* Make an opening to propose sequencing only SSU 16S V4V5 hypervariable regions and deepen the bioanalysis with this approach: if it provides the same information for symbiosis association, why should we keep sequencing SSU 18S v9 of the hosts?





# Bug
1: dans le script awk `LCA.awk`: ajouter *OFS="\t"* dans le *BEGIN*. Autrement, dans l'assignation taxo, on retrouve des tabulations (qui sont aussi nos séparateurs de champs).


2: `16s_v4v5_no_overlaped/R/dada_denoising.R` 
*'merger <- mergePairs(ddF, derepF, ddR, derepR, minOverlap=0)'*. 
Nous avons fait ça afin de faire fusionner les R1 et R2, sans chevauchement. Le problème était que ça nous générait des séquences ou les R1 et R2 étaient inversés. Le problème est résolu avec 
*'merger <- mergePairs(ddF, derepF, ddR, derepR, justConcatenate=TRUE)'*
en faisait des blast avec des séquences présentes dans le résultat final:` finalresult/document/`asv_table_v4v5_v9_and_v4v5_no_overlaped_pr2, on se rend compte que la query sequence est bien aligné en 5' -> 3' sur la base de ref. Pourquoi ? À creuser ...
    
     
